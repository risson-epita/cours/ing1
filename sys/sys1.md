# Sys1

Système d'exploitation : aide aux programmes, s'occupe de gérer la complexité
d'un système :
* gestion des ressources pour les programmes
* fournir des interfaces et des abstractions pour les programmes utilisateurs

## Architecture d'un ordinateur

De quoi est fait un ordinateur :
1 CPU (1 cœur) : exécute des instructions
RAM
devices (contrôleur clavier, disques)
Contrôleur mémoire reliant les éléments précédents

Registres, de petite zone de mémoire temporaire ~8 octets :
* registres généraux (additions..., lire & écrire en mémoire)
* registres de contrôle, permettent de configurer le processeur (et la machine)

CPU Modes :
* user mode (ring3)
* supervisor mode (ring0)

## Noyau de système d'exploitation (ou kernel)

Fourni des interfaces aux programmes utilisateurs, via des appels système
(syscalls). 

Lors de l'exécution d'un syscall, il se passe un « contexte switch » ()avec
changement de mode).
On exécute pas de syscall directement en C, mais on passe par un wrapper
(généralement exposé par la libc).

## Fichiers

« Tout est un fichier »

i-node ou inode :
* contient toute les informations d'un fichier
* numéro d'inode : « adresse » qui permet de retrouver le fichier

Ce qu'on trouve dans un inode :
* où sont les données
* type de fichier
* permissions, uid/gid
* dates de création, modification, …
* taille

Pour trouver ces informations : stat/fstat

Types de fichiers :
* regular
* directory
* liens symboliques
* special files : char device, block device
* sockets
* named pipe (FIFO)

### Directory walk

`opendir`/`readdir`/`closedir`

## Comment exécuter un nouveau programme ?

Programme : fichier qui contient du code
Process : truc qui s'exécute, la plupart du temps, le contenu d'un programme

Syscall `fork()` : duplique le process courant
Syscall `wait*()` : `wait()`, `waitpid()`, …

```sh
42sh$ find . -name "*.pyc" -exec rm {} \;
```

Syscall `execve()` : créer un nouvel espace mémoire dans le process courant

```c
pid_t pid = fork();
if (pid < 0)
    err(1, "unable to fork");
if (pid)
{
    int rc = waitpid(pid/*…*/);
    // error checking, retry, etc
    return;
}

char *argv[] = { "rm", "toto.pyc", NULL };
execvp(argv[0], argv);
err(1, "error");
```

