#include <assert.h>
#include <stdio.h>

unsigned int int_width(int i)
{
    unsigned int width = 0;
    if (i <= 0)
        ++width;
    while (i != 0)
    {
        i /= 10;
        ++width;
    }
    return width;
}

unsigned int ints_width(const int *tab, unsigned int count)
{
    unsigned int king = int_width(tab[0]);
    for (unsigned int i = 1; i < count; ++i)
    {
        unsigned int current_int_width = int_width(tab[i]);
        if (current_int_width > king)
            king = current_int_width;
    }
    return king;
}

void print_int_array(FILE *out, const int *tab, unsigned int count)
{
    unsigned int width = ints_width(tab, count);
    unsigned int index_width = int_width(count - 1);
    int char_count = 0;
    for (unsigned int i = 0; i < count; ++i)
    {
        if (char_count == 0)
        {
            char_count += fprintf(out, "%*s[%u]", index_width - int_width(i), "", i);
        }
        if (char_count + width + 1 > 80)
        {
            fprintf(out, "\n");
            --i;
            char_count = 0;
            continue;
        }
        char_count += fprintf(out, " %*d", width, tab[i]);
    }
    fprintf(out, "\n");
}

void insert_sort(int* tab, unsigned count)
{
    for (unsigned int i = 1; i < count; ++i)
    {
        int key = tab[i];
        unsigned int j = i;
        for (; j > 0 && tab[j - 1] > key; --j)
            tab[j] = tab[j - 1];
        tab[j] = key;
    }
}

void insert_sort_cmp(int* tab, unsigned count, int (*cmp)(int a, int b))
{
    for (unsigned int i = 1; i < count; ++i)
    {
        int key = tab[i];
        unsigned int j = i;
        for (; j > 0 && cmp(tab[j - 1], key) == 1; --j)
            tab[j] = tab[j - 1];
        tab[j] = key;
    }
}

unsigned linear_search(const int* tab, unsigned count, int val, int (*cmp)(int a, int b))
{
    for (unsigned int i = 0; i < count; ++i)
    {
        int rc = cmp(val, tab[i]);
        if (rc == 0 || rc == -1)
            return i;
    }
    return count;
}

unsigned binary_search(int* tab, unsigned count, int val, int (*cmp)(int a, int b))
{
    unsigned int begin = 0;
    unsigned int end = count;
    while (begin < end)
    {
        unsigned int middle = begin + (end - begin) / 2;
        int rc = cmp(val, tab[middle]);
        if (rc == 0)
            return middle;
        if (rc == -1)
            end = middle;
        else
            begin = middle + 1;
    }
    return begin;
}

void bs_insert_sort_cmp(int *tab, unsigned count, int (*cmp)(int a, int b))
{
    for (unsigned int i = 1; i < count; ++i)
    {
        int key = tab[i];
        unsigned int pos = binary_search(tab, i, key, cmp);
        for (long k = i - 1; k >= pos; --k)
            tab[k + 1] = tab[k];
        tab[pos] = key;
    }
}

long long clamp(long long x, long long b, long long e)
{
   if (x < b)
      return b;
   if (x > e)
      return e;
   return x;
}

unsigned interpolate(int *tab, unsigned begin, unsigned end, int val, int (*cmp)(int, int))
{
  if (tab[end] < val)
    return end;
  if (tab[begin] > val)
    return begin;
   long long b = begin;
   long long e = end;
   if (cmp(tab[b], tab[e]) == 0)
   {
      if (cmp(tab[b], val) == -1)
         return e;
      else
         return b;
   }
   return clamp(b + (((long double)(e - b) / (tab[e] - tab[b]) * (val - tab[b]))), begin, end);
}

unsigned interpolation_search(int *tab, unsigned count, int val, int (*cmp)(int a, int b))
{
    if (count == 0)
        return count;
    long long begin = 0;
    long long end = count - 1;
    while (begin <= end && tab[begin] <= val && val <= tab[end])
    {
        long long middle = interpolate(tab, begin, end, val, cmp);
        int rc = cmp(val, tab[middle]);
        if (rc == 0)
            return middle;
        if (rc == -1)
            end = clamp(middle - 1, 0, count - 1);
        else
            begin = clamp(middle + 1, 0, count - 1);
    }
    return begin;
}

unsigned cmp_count;

int increasing(int a, int b)
{
   ++cmp_count;
   if (a < b)
     return -1;
   return a > b;
}

#define my_test(val) \
  cmp_count = 0; \
  printf("interpolation_search(a, %u, %d, increasing) = %u\n", \
         asize, (val), interpolation_search(a, asize, (val), increasing)); \
  printf("\twith %u comparisons\n", cmp_count);

int main()
{
  int a[] = { 1, 2, 3, 4, 5, 6, 9, 12, 15, 20, 25, 35, 38, 40, 41 };
  unsigned asize = sizeof(a) / sizeof(*a);
  puts("a[]:");
  print_int_array(stdout, a, asize);
  my_test(0);
  my_test(6);
  my_test(8);
  my_test(20);
  my_test(41);
  my_test(42);
  my_test(-2147483647 - 1);
  my_test(2147483647);
  return 0;
}
