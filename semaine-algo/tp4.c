#include <stdlib.h>
#include <stdio.h>

unsigned int int_width(int i)
{
    unsigned int width = 0;
    if (i <= 0)
        ++width;
    while (i != 0)
    {
        i /= 10;
        ++width;
    }
    return width;
}

unsigned int ints_width(const int *tab, unsigned int count)
{
    unsigned int king = int_width(tab[0]);
    for (unsigned int i = 1; i < count; ++i)
    {
        unsigned int current_int_width = int_width(tab[i]);
        if (current_int_width > king)
            king = current_int_width;
    }
    return king;
}

void print_int_array(FILE *out, const int *tab, unsigned int count)
{
    unsigned int width = ints_width(tab, count);
    unsigned int index_width = int_width(count - 1);
    int char_count = 0;
    for (unsigned int i = 0; i < count; ++i)
    {
        if (char_count == 0)
        {
            char_count += fprintf(out, "%*s[%u]", index_width - int_width(i), "", i);
        }
        if (char_count + width + 1 > 80)
        {
            fprintf(out, "\n");
            --i;
            char_count = 0;
            continue;
        }
        char_count += fprintf(out, " %*d", width, tab[i]);
    }
    fprintf(out, "\n");
}

void swap(int *tab, unsigned i, unsigned j)
{
    int tmp = tab[j];
    tab[j] = tab[i];
    tab[i] = tmp;
}

unsigned partition_cmp(int* tab, unsigned b, unsigned e,
                       unsigned p, int (*cmp)(int a, int b))
{
    int pivot = tab[p];
    unsigned i = b - 1;
    unsigned j = e;
    while (1)
    {
        do
            ++i;
        while (cmp(tab[i], pivot) == -1);

        do
            --j;
        while (cmp(tab[j], pivot) == 1);

        if (j <= i)
            return i + (i == b ? 1 : 0);

        swap(tab, i, j);
    }
}

unsigned pivot_first(const int* tab, unsigned l, unsigned r,
                     int (*cmp)(int a, int b))
{
  (void) tab;
  (void) r;
  (void) cmp;
  return l;
}

unsigned pivot_rand(const int* tab, unsigned l, unsigned r,
                    int (*cmp)(int a, int b))
{
  (void) tab;
  return l + (rand() % (r - l));
}

unsigned pivot_median3(const int* tab, unsigned l, unsigned r,
                       int (*cmp)(int a, int b))
{
    unsigned left_i = l;
    unsigned middle_i = l + (r - l) / 2;
    unsigned right_i = r - 1;
    int left = tab[left_i];
    int middle = tab[middle_i];
    int right = tab[right_i];

    int left_right = cmp(left, right);
    int left_middle = cmp(left, middle);
    int middle_right = cmp(middle, right);

    if (left_middle != 1)
    {
        if (middle_right != 1)
            return middle_i;
        if (left_right != 1)
            return right_i;
        return left_i;
    }
    else
    {
        if (middle_right == 1)
            return middle_i;
        if (left_right != 1)
            return left_i;
        return right_i;
    }
}

void quick_sort_cmp_rec(int* tab, unsigned b, unsigned e,
                        unsigned (*pivot)(const int* tab, unsigned l, unsigned r,
                                          int (*cmp)(int a, int b)),
                        int (*cmp)(int a, int b))
{
    if ((e - b) > 1)
    {
        unsigned pivot_i = pivot(tab, b, e, cmp);
        unsigned p = partition_cmp(tab, b, e, pivot_i, cmp);
        quick_sort_cmp_rec(tab, b, p, pivot, cmp);
        quick_sort_cmp_rec(tab, p, e, pivot, cmp);
    }
}

void quick_sort_cmp(int* tab, unsigned count,
                    unsigned (*pivot)(const int* tab, unsigned l, unsigned r,
                                      int (*cmp)(int a, int b)),
                    int (*cmp)(int a, int b))
{
    quick_sort_cmp_rec(tab, 0, count, pivot, cmp);
}
